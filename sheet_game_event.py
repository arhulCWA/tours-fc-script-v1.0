import requests
from connection import establish_db_connection
import json as j
import locale
from sheet_game import getSheetGameIdByGameNo


def insertSheetGameEvent(gameNo):
    connection = establish_db_connection()
    cursor = connection.cursor()
    match = requests.get("https://api-dofa.fff.fr/api/match_entities/" + str(gameNo)).json()
    match_feuille = requests.get("https://api-dofa.fff.fr" + str(
        requests.get("https://api-dofa.fff.fr/api/match_entities/" + str(gameNo)).json()["match_feuille"])).json()
    # print("feuille de match :" + str(gameNo))
    # print(match_feuille)
    if match_feuille is not None:
        for event in match_feuille.get("match_feuille_evenements"):
            type = "goal"
            # print(match)
            # print(match["home_score"])
            # print(match["away_score"])
            if match["home_score"] == 0 and match["away_score"] == 0:
                type = "penalty"
            if event["cfe_typ"] == "BU":
                # print(type)
                insert_querry = "INSERT INTO app_page_sheet_game_event (minute, clubNumber, player, detail, pageSheetGame_id, type, eventImage_id) VALUES (%s,%s,%s,%s,%s,%s,%s)"
                data_to_insert = (
                    event["cfe_min"],
                    event["l1_li_no"]["cfi_cl_no"],
                    event["l1_li_no"]["in_prenom"] + " " + event["l1_li_no"]["in_nom"],
                    None,
                    getSheetGameIdByGameNo(gameNo),
                    type,
                    None
                )
                # print(data_to_insert)
                cursor.execute(insert_querry, data_to_insert)
    connection.commit()
    cursor.close()
    connection.close()


def isSheetGameEventIsInDb(gameNo):
    sheetgameId = getSheetGameIdByGameNo(gameNo)
    if sheetgameId is None:
        return False
    connection = establish_db_connection()
    cursor = connection.cursor()
    select_query = "SELECT id FROM app_page_sheet_game_event WHERE pageSheetGame_id = %s"
    cursor.execute(select_query, [sheetgameId])
    data = cursor.fetchone()
    connection.commit()
    cursor.close()
    connection.close()
    if data is None:
        return False
    else:
        return True


# insertSheetGameEvent(27227868)
