import requests
from connection import establish_db_connection
from teams import findIdByTeam
from season import findSeasonByStartYear


def rankingExist(seasonId):
    connection = establish_db_connection()
    cursor = connection.cursor(buffered=True)
    select_query = "SELECT * FROM app_admin_ranking WHERE season_id = %s"

    cursor.execute(select_query, [findSeasonByStartYear(seasonId)])
    data = cursor.fetchone()
    cursor.close()
    connection.close()
    if data is None:
        return False
    else:
        return True





def findRankingByTeamNumber(teamNumber):
    connection = establish_db_connection()
    cursor = connection.cursor(buffered=True)
    select_query = "SELECT id FROM app_admin_ranking WHERE team_id= %s"
    cursor.execute(select_query, [findIdByTeam(teamNumber)])
    data = cursor.fetchone()
    cursor.close()
    connection.close()
    if data is None:
        return False
    else:
        return data[0]


def insertRanking(seasonDate):
    connection = establish_db_connection()
    cursor = connection.cursor(buffered=True)

    url = "https://api-dofa.fff.fr/api/compets/407672/phases/1/poules/3/classement_journees"

    ranking = requests.get(url).json().get("hydra:member", [])
    for rank in ranking:
        insert_query = "INSERT INTO app_admin_ranking (team_id, season_id, `rank`, points, goalFor, goalAgainst, wonGames, lostGames, drawGames) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s)"

        values = (
            findIdByTeam(rank["equipe"]["club"]["cl_no"]),
            findSeasonByStartYear(rank["season"]),
            rank["rank"],
            rank["point_count"],
            rank["goals_for_count"],
            rank["goals_against_count"],
            rank["won_games_count"],
            rank["lost_games_count"],
            rank["draw_games_count"],
        )

        cursor.execute(insert_query, values)
        connection.commit()
    cursor.close()
    connection.close()


def updateRanking(seasonDate):
    connection = establish_db_connection()
    cursor = connection.cursor(buffered=True)

    url = "https://api-dofa.fff.fr/api/compets/407672/phases/1/poules/3/classement_journees"

    ranking = requests.get(url).json().get("hydra:member", [])

    for rank in ranking:
        insert_query = "UPDATE app_admin_ranking SET `rank`=%s, points=%s, goalFor=%s, goalAgainst=%s, wonGames=%s, lostGames=%s, drawGames=%s WHERE id = %s and season_id = %s "

        values = (
            rank["rank"],
            rank["point_count"],
            rank["goals_for_count"],
            rank["goals_against_count"],
            rank["won_games_count"],
            rank["lost_games_count"],
            rank["draw_games_count"],
            findRankingByTeamNumber(rank["equipe"]["club"]["cl_no"]),
            findSeasonByStartYear(seasonDate)
        )
        print("update " + str(findRankingByTeamNumber(rank["equipe"]["club"]["cl_no"])))
        cursor.execute(insert_query, values)
        connection.commit()
    cursor.close()
    connection.close()
