# Créer un application Python

## Ajouter un nom et un url

<img src="../script/README/C1.png">

## Utiliser la console pour cloner le projet

```bash
source /home/cwanet/virtualenv/"Nom du projet"/3.11/bin/activate && cd /home/cwanet/"Nom du projet"
```

```bash
git clone "url du projet"
```

## Ajouter les requirements

### remplacer "script_toursfc/toursfc-script/requirements.txt" par le chemin de votre fichier requirements

<img src="../script/README/C2.png">

### Cliquer sur "Run Pip Install"

## Mettre les données pour la connection à la base de données

### Copier le fichier data_example.py et le renommer en data.py

### Changer les informations dans le data.py

## Crée les crontab

<img src="../script/README/C3.png">

