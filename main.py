from insert_all import insertAll
from ranking import rankingExist
from ranking import insertRanking
from ranking import updateRanking
from season import findActiveSeasonYear

insertAll("https://api-dofa.fff.fr/api/compets/407672/phases/1/poules/3/poule_journees?details[]=pouleJourneeWithMatch")
insertAll("https://api-dofa.fff.fr/api/compets/405935/phases/1/poules/3/poule_journees?details[]=pouleJourneeWithMatch")
insertAll("https://api-dofa.fff.fr/api/compets/410876/phases/1/poules/3/poule_journees?details[]=pouleJourneeWithMatch")
season = findActiveSeasonYear()
if rankingExist(season):
    updateRanking(season)
    print("update ranking")
else:
    insertRanking(season)
    print("insert ranking")