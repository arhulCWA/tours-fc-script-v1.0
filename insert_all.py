# -*- coding: utf-8 -*-

import requests

from games import insertGame
from games import gameIsInDb
from games import updateGame
from games import findIdByGameNo
from teams import insertTeam
from teams import teamIsInDb
from teams import findIdByTeam
from game_team import insertGameTeam

from sheet_game import IsSheetGamePageExistInDb
from sheet_game import createSheetGamePage

from mailer import sendMail
from sheet_game_event import insertSheetGameEvent
from games import isGameFinish
from sheet_game_event import isSheetGameEventIsInDb


def insertAll(url):
    response = requests.get(url)

    pouleJournees = response.json().get("hydra:member", [])

    for pouleJournee in pouleJournees:
        for game in pouleJournee.get("matchs"):
            if game["home"]["club"]["@id"] == "/api/clubs/2440" or game["away"]["club"]["@id"] == "/api/clubs/2440":
                print("for game " + str(game["ma_no"]) + ":")
                if not gameIsInDb(game["ma_no"]):
                    ids = insertGame(game["ma_no"])
                    print("insert game")
                    if not teamIsInDb(ids[0]):
                        team = insertTeam(ids[0])
                        sendMail("Nouvelle équipe ajouté : " + team, 'a.rhul@creatiswebart.com')
                        sendMail("Nouvelle équipe ajouté : " + team, 'c.chapleau@creatiswebart.com')
                        print("insert team " + team)
                    if not teamIsInDb(ids[1]):
                        team = insertTeam(ids[1])
                        sendMail("Nouvelle équipe ajouté : " + team, 'a.rhul@creatiswebart.com')
                        sendMail("Nouvelle équipe ajouté : " + team, 'c.chapleau@creatiswebart.com')
                        print("insert team " + team)
                    insertGameTeam([findIdByGameNo(game["ma_no"]), findIdByTeam(ids[0])])
                    insertGameTeam([findIdByGameNo(game["ma_no"]), findIdByTeam(ids[1])])

                    if isGameFinish(game["ma_no"]) and not isSheetGameEventIsInDb(game["ma_no"]):
                        createSheetGamePage(game["ma_no"])
                        insertSheetGameEvent(game["ma_no"])
                else:
                    updateGame(game["ma_no"])
                    if not IsSheetGamePageExistInDb(game["ma_no"]):
                        if isGameFinish(game["ma_no"]) and not isSheetGameEventIsInDb(game["ma_no"]):
                            createSheetGamePage(game["ma_no"])
                            insertSheetGameEvent(game["ma_no"])
                        print("creation page")
                    print("update faite")
                # if gameSheetExist(game["ma_no"]):
                #     if gameSheetExistInDb(game["ma_no"]):
                #         updateGameSheet(game["ma_no"])
                #         print("update gamesheet")
                #     else:
                #         insertGameSheet(game["ma_no"])
                #         print("insert gamesheet")
                # else:
                #     print("pas de sheetgame")
