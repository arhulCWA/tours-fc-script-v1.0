
import requests
import mysql.connector
import json

#Constantes du scripts (non presant dans le git)
import data

def establish_db_connection():
    host = data.host
    user = data.user
    password = data.password
    database = data.database
    return mysql.connector.connect(
        host=host,
        user=user,
        password=password,
        database=database
    )
connection = establish_db_connection()
cursor = connection.cursor()

url = "https://api-dofa.fff.fr/api/compets/407672/phases/1/poules/3/classement_journees"

response = requests.get(url)

members = response.json().get("hydra:member",[])

#Verification des équipes
select_query = "SELECT id,clubNumber FROM app_admin_team"
cursor.execute(select_query)

# rawData est sous cette forme [(1, 2446), (2, 127721),...]
rawData = cursor.fetchall()

# ids des champs dans la bd
sqlids = [item[0] for item in rawData]

# no de clubs récuperer depuis la db
data = [item[1] for item in rawData]

#no de club récuperer depuis l'API
ids = []
for member in members :
    ids.append(member['equipe']['club']['cl_no'])
for i in range(len(ids)):
        if(ids[i] not in data):
            print("NEW INSERT")
            club = requests.get("https://api-dofa.fff.fr"+members[i]['equipe']['club']['@id']).json()
            insert_query = "INSERT INTO app_admin_team (enabled,clubNumber,name,stadium,address,zipcode,city,lat,lng) VALUES (%s, %s,%s,%s,%s,%s,%s,%s,%s)"
            if(club.get("address1") == None):
                address = club.get("address2")
            else:
                address = club.get("address1")
            if(club.get("latitude")== None or club.get("longitude") == None):
                lat = 0
                long = 0
            else :
                lat = club.get("latitude")
                long = club.get("longitude")
            data_to_insert= (
                False,
                ids[i],
                club.get("name"),
                (club.get("terrains",[])[0]["name"]),
                address,
                club.get("postal_code"),
                club.get("location"),
                lat,
                long
                )
            cursor.execute(insert_query,data_to_insert)  
            connection.commit()
        """    
        else:
            print("UPDATE")
            club = requests.get("https://api-dofa.fff.fr"+members[i]['equipe']['club']['@id']).json()
            insert_query = "UPDATE app_admin_team SET enabled= %s,clubNumber= %s,name= %s,stadium= %s,address= %s,zipcode= %s,city= %s,lat= %s,lng= %s WHERE id = %s"
            if(club.get("address1") == None):
                address = club.get("address2")
            else:
                address = club.get("address1")
            if(club.get("latitude")== None or club.get("longitude") == None):
                lat = 0
                long = 0
            else :
                lat = club.get("latitude")
                long = club.get("longitude")
            data_to_insert= (
                False,
                ids[i],
                club.get("name"),
                (club.get("terrains",[])[0]["name"]),
                address,
                club.get("postal_code"),
                club.get("location"),
                lat,
                long,
                sqlids[i]
                )
            cursor.execute(insert_query,data_to_insert)  
            connection.commit()
            """
cursor.close()        
connection.close() 