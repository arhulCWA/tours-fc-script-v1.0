import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import base64


# Email configuration
def sendMail(content, receiver_email):
    sender_email = 'no-reply@creatiswebart.com'
    subject = '[AUTO] TOURS FC : database insert'

    with open("./logo-tours-fc.svg", 'rb') as image_file:
        image_data = base64.b64encode(image_file.read()).decode('utf-8')

    message = f"""
                <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
                <html lang="fr">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                        <meta name="viewport" content="width=device-width">
                    </head>
                    <body style="margin:0; padding:0;">
                    <img src="data:image/svg+xml;base64,{image_data}" alt="Your SVG Image Alt Text">
                        {content}
                    </body>
                </html>
              """

    # Create a MIMEText object for the email content
    msg = MIMEMultipart()
    msg['From'] = sender_email
    msg['To'] = receiver_email
    msg['Subject'] = subject

    # Attach the message to the email
    msg.attach(MIMEText(message, 'html'))

    smtp_port = 465
    smtp_server = 'hybrid3563.fr.ns.planethoster.net'
    smtp_username = 'no-reply@creatiswebart.net'
    smtp_password = 'CtA7bWzwxUx6//I0e6MRlMFLwv'
    # Create an SMTP server object and send the email
    try:
        server = smtplib.SMTP_SSL(smtp_server, smtp_port)
        server.login(smtp_username, smtp_password)
        server.sendmail(sender_email, receiver_email, msg.as_string())
        print("email sent")
    except Exception as e:
        print('Email could not be sent. Error:', str(e))
    finally:
        server.quit()