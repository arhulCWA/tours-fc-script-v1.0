# -*- coding: utf-8 -*-
import requests
from connection import establish_db_connection
import json as j
import locale
from datetime import datetime

from mailer import sendMail


# Verification des matchs
def gameIsInDb(gameNo):
    connection = establish_db_connection()
    cursor = connection.cursor()
    select_query = "SELECT id FROM app_admin_game WHERE matchNumber = %s"
    cursor.execute(select_query, [gameNo])
    data = cursor.fetchone()
    cursor.close()
    connection.close()
    if data is None:
        return False
    else:
        return True

def isGameFinish(gameNo):
    connection = establish_db_connection()
    cursor = connection.cursor()
    select_query = "SELECT schedule,isStop FROM app_admin_game WHERE matchNumber = %s"
    cursor.execute(select_query, [gameNo])
    data = cursor.fetchone()
    cursor.close()
    connection.close()
    current_datetime = datetime.now()
    if data[0] < current_datetime and not data[1]:
        return True
    else:
        return False

def insertGame(gameNo):
    connection = establish_db_connection()
    cursor = connection.cursor()

    url = "https://api-dofa.fff.fr/api/match_entities/" + str(gameNo)

    response = requests.get(url)
    match = response.json()

    insert_query = "INSERT INTO app_admin_game (schedule, officials, stadium, matchNumber,clubNumberHome,clubNumberAway,competition,season,round,scoreHome,scoreAway,sheetGame,penaltyHome,penaltyAway,isStop) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
    mbs = []
    membres = {"match_membres": []}
    for membre in match.get("match_membres"):
        mbs.append({
            "@id": membre["@id"],
            "prenom": membre["prenom"],
            "nom": membre["nom"],
            "label_position": membre["label_position"],
        })
    locale.setlocale(locale.LC_TIME, 'fr_FR')
    isStop = False
    if match["ma_arret"] == "O" :
        isStop = True

    for mb in mbs:
        if mb["label_position"] == "Arbitre centre":
            membres["match_membres"].append(mb)
            break
    for mb in mbs:
        if mb["label_position"] == "Arbitre assistant 1":
            membres["match_membres"].append(mb)
            break
    for mb in mbs:
        if mb["label_position"] == "Arbitre assistant 2":
            membres["match_membres"].append(mb)
            break
    for mb in mbs:
        if mb["label_position"] == "Délégué principal":
            membres["match_membres"].append(mb)
            break
    for mb in mbs:
        if mb["label_position"] == "Délégué adjoint 1":
            membres["match_membres"].append(mb)
            break
    for mb in mbs:
        if mb["label_position"] == "Délégué adjoint 2":
            membres["match_membres"].append(mb)
            break

    feuille = None
    if match["match_feuille"] is not None:
        match_feuille = requests.get("https://api-dofa.fff.fr" + str(match["match_feuille"])).json()
        feuille = {"players": [],
                   "goals": []}

        for player in match_feuille.get("match_feuille_compos"):
            if player["cfi_cl_no"] == 2440:
                feuille["players"].append({
                    "role": player["cfi_role"],
                    "maillot": player["cfi_maillot"],
                    "nom": player["in_nom"],
                    "prenom": player["in_prenom"],
                    "cap": player["cfi_capi"]
                })

        for event in match_feuille.get("match_feuille_evenements"):
            if event["cfe_typ"] == "BU":
                feuille["goals"].append({
                    "min": event["cfe_min"],
                    "club": event["l1_li_no"]["cfi_cl_no"],
                    "player": {
                        "number": str(event["l1_li_no"]["cfi_maillot"]),
                        "lastname": event["l1_li_no"]["in_nom"],
                        "firstname": event["l1_li_no"]["in_prenom"],
                    }
                })
        if match["match_feuille"] is not None:
            feuille = j.dumps(feuille)
    else:
        if match["date"] < datetime.now():
            sendMail("Attention : Feuille de match vide", 'a.rhul@creatiswebart.com')
    data_to_insert = (
        match.get("date").split('T')[0] + " " + match.get("time").replace("H", ":") + ":00",
        j.dumps(membres["match_membres"]),
        j.dumps(match.get("terrain")),
        match.get("ma_no"),
        match.get("home")["club"]["cl_no"],
        match.get("away")["club"]["cl_no"],
        match.get("competition")["name"],
        match.get("competition")["season"],
        match.get("poule_journee")["number"],
        match.get("home_score"),
        match.get("away_score"),
        feuille,
        match.get("home_nb_tir_but"),
        match.get("away_nb_tir_but"),
        isStop

    )
    cursor.execute(insert_query, data_to_insert)
    connection.commit()
    cursor.close()
    connection.close()
    return [match.get("home")["club"]["cl_no"],
            match.get("away")["club"]["cl_no"]]


def updateGame(gameNo):
    connection = establish_db_connection()
    cursor = connection.cursor()

    url = "https://api-dofa.fff.fr/api/match_entities/" + str(gameNo)

    response = requests.get(url)
    match = response.json()

    insert_query = "UPDATE app_admin_game SET schedule=%s,officials=%s, stadium=%s, matchNumber=%s,clubNumberHome=%s,clubNumberAway=%s,competition=%s,season=%s,round=%s,scoreHome=%s,scoreAway=%s,sheetGame=%s,penaltyHome=%s,penaltyAway=%s,isStop=%s WHERE matchNumber = %s"
    membres = {"match_membres": []}
    mbs = []

    isStop = False
    if match["ma_arret"] == "O":
        isStop = True
    for membre in match.get("match_membres"):
        mbs.append({
            "@id": membre["@id"],
            "prenom": membre["prenom"],
            "nom": membre["nom"],
            "label_position": membre["label_position"],
        })
    locale.setlocale(locale.LC_TIME, 'fr_FR')

    for mb in mbs:
        if mb["label_position"] == "Arbitre centre":
            membres["match_membres"].append(mb)
            break
    for mb in mbs:
        if mb["label_position"] == "Arbitre assistant 1":
            membres["match_membres"].append(mb)
            break
    for mb in mbs:
        if mb["label_position"] == "Arbitre assistant 2":
            membres["match_membres"].append(mb)
            break
    for mb in mbs:
        if mb["label_position"] == "Délégué principal":
            membres["match_membres"].append(mb)
            break
    for mb in mbs:
        if mb["label_position"] == "Délégué adjoint 1":
            membres["match_membres"].append(mb)
            break
    for mb in mbs:
        if mb["label_position"] == "Délégué adjoint 2":
            membres["match_membres"].append(mb)
            break

    feuille = None
    if match["match_feuille"] is not None:
        match_feuille = requests.get("https://api-dofa.fff.fr" + str(match["match_feuille"])).json()
        feuille = {"players": [],
                   "goals": []}

        for player in match_feuille.get("match_feuille_compos"):
            if player["cfi_cl_no"] == 2440:
                feuille["players"].append({
                    "role": player["cfi_role"],
                    "maillot": player["cfi_maillot"],
                    "nom": player["in_nom"],
                    "prenom": player["in_prenom"],
                    "cap": player["cfi_capi"]
                })

        for event in match_feuille.get("match_feuille_evenements"):
            if event["cfe_typ"] == "BU":
                feuille["goals"].append({
                    "min": event["cfe_min"],
                    "club": event["l1_li_no"]["cfi_cl_no"],
                    "player": {
                        "number": str(event["l1_li_no"]["cfi_maillot"]),
                        "lastname": event["l1_li_no"]["in_nom"],
                        "firstname": event["l1_li_no"]["in_prenom"],
                    }
                })
        if match["match_feuille"] is not None:
            feuille = j.dumps(feuille)
    else:
        if datetime.strptime(match["date"].split('T')[0],"%Y-%m-%d") < datetime.now():
            sendMail("Attention : Feuille de match vide", 'a.rhul@creatiswebart.com')
            sendMail("Attention : Feuille de match vide", 'c.chapleau@creatiswebart.com')
    data_to_insert = (
        match.get("date").split('T')[0] + " " + match.get("time").replace("H", ":") + ":00",
        j.dumps(membres["match_membres"]),
        j.dumps(match.get("terrain")),
        match.get("ma_no"),
        match.get("home")["club"]["cl_no"],
        match.get("away")["club"]["cl_no"],
        match.get("competition")["name"],
        match.get("competition")["season"],
        match.get("poule_journee")["number"],
        match.get("home_score"),
        match.get("away_score"),
        feuille,
        match.get("home_nb_tir_but"),
        match.get("away_nb_tir_but"),
        isStop,
        gameNo
    )
    cursor.execute(insert_query, data_to_insert)
    connection.commit()
    cursor.close()
    connection.close()


def findIdByGameNo(gameNo):
    connection = establish_db_connection()
    cursor = connection.cursor()

    select_query = "SELECT id FROM app_admin_game WHERE matchNumber = %s"
    cursor.execute(select_query, [gameNo])
    data = cursor.fetchone()
    connection.commit()
    cursor.close()
    connection.close()
    return data[0]


def gameHasSheetGame(gameNo):
    connection = establish_db_connection()
    cursor = connection.cursor()
    select_query = "SELECT sheetGame FROM app_admin_game WHERE matchNumber = %s"
    cursor.execute(select_query, [gameNo])
    data = cursor.fetchone()
    connection.commit()
    cursor.close()
    connection.close()
    if data is None:
        return False
    else:
        return True
