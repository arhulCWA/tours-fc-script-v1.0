from connection import establish_db_connection

def findSeasonByStartYear(seasonYear):
    connection = establish_db_connection()
    cursor = connection.cursor()
    select_query = "SELECT id FROM app_admin_season WHERE startDate = %s"
    cursor.execute(select_query, [seasonYear])
    data = cursor.fetchone()
    cursor.close()
    connection.close()
    if data is None:
        return False
    else:
        return data[0]


def findActiveSeasonYear():
    connection = establish_db_connection()
    cursor = connection.cursor()
    select_query = "SELECT startDate FROM app_admin_season WHERE enabled= 1"
    cursor.execute(select_query)
    data = cursor.fetchone()
    cursor.close()
    connection.close()
    return data[0]