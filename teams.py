import requests
from connection import establish_db_connection


def teamIsInDb(cl_no):
    connection = establish_db_connection()
    cursor = connection.cursor(buffered=True)
    select_query = "SELECT id FROM app_admin_team WHERE clubNumber = %s"
    cursor.execute(select_query, [cl_no])
    data = cursor.fetchone()
    cursor.close()
    connection.close()
    if data is None:
        return False
    else:
        return True


def insertTeam(cl_no):
    connection = establish_db_connection()
    cursor = connection.cursor(buffered=True)

    url = "https://api-dofa.fff.fr/api/clubs/" + str(cl_no)

    response = requests.get(url)
    club = response.json()

    insert_query = "INSERT INTO app_admin_team (enabled,clubNumber,name,stadium,address,zipcode,city,lat,lng,verified) VALUES (%s, %s,%s,%s,%s,%s,%s,%s,%s,%s)"
    if club.get("address1") is None:
        address = club.get("address2")
    else:
        address = club.get("address1")
    if club.get("latitude") is None or club.get("longitude") is None:
        lat = 0
        long = 0
    else:
        lat = club.get("latitude")
        long = club.get("longitude")
    data_to_insert = (
        False,
        club.get("cl_no"),
        club.get("name"),
        (club.get("terrains", [])[0]["name"]),
        address,
        club.get("postal_code"),
        club.get("location"),
        lat,
        long,
        0
    )
    cursor.execute(insert_query, data_to_insert)
    connection.commit()

    select_query = "SELECT LAST_INSERT_ID() FROM app_admin_team"
    cursor.execute(select_query)
    teamId = cursor.fetchone()[0]

    insert_query = "INSERT INTO app_admin_team_image (owner_id,type,path) VALUES (%s,%s,%s)"

    data_to_insert = (teamId, "default", club.get("logo"))
    cursor.execute(insert_query, data_to_insert)
    connection.commit()
    cursor.close()
    connection.close()
    return club.get("name")


def findIdByTeam(teamNo):
    connection = establish_db_connection()
    cursor = connection.cursor(buffered=True)

    select_query = "SELECT id FROM app_admin_team WHERE clubNumber = %s"
    cursor.execute(select_query, [teamNo])
    data = cursor.fetchone()
    connection.commit()
    cursor.close()
    connection.close()
    return data[0]
