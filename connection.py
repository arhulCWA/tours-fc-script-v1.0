import mysql.connector

import data


def establish_db_connection():
    host = data.host
    user = data.user
    password = data.password
    database = data.database
    return mysql.connector.connect(
        host=host,
        user=user,
        password=password,
        database=database
    )
