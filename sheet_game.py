import requests
from connection import establish_db_connection
import json as j
import locale
from datetime import datetime
from games import findIdByGameNo
from teams import findIdByTeam
import re


def IsSheetGamePageExistInDb(gameNo):
    connection = establish_db_connection()
    cursor = connection.cursor()
    select_query = "SELECT id FROM app_page_sheet_game WHERE game_id = %s"
    cursor.execute(select_query, [findIdByGameNo(gameNo)])

    data = cursor.fetchone()
    connection.commit()
    cursor.close()
    connection.close()
    if data is None:
        return False
    else:
        return True


# Version optimisée (non testé)
# def IsSheetGamePageExistInDb(gameNo):
#     with establish_db_connection() as connection:
#         with connection.cursor() as cursor:
#             select_query = "SELECT EXISTS (SELECT 1 FROM app_page_sheet_game WHERE game_id = %s)"
#             cursor.execute(select_query, [findIdByGameNo(gameNo)])
#             return cursor.fetchone()[0]

def getSheetGameIdByGameNo(gameNo):
    connection = establish_db_connection()
    cursor = connection.cursor(buffered=True)
    # match_feuille = requests.get("https://api-dofa.fff.fr/api/match_entities/" + str(gameNo)).json()["match_feuille"]
    # number = match_feuille.split("/")[-1]
    gameId = findIdByGameNo(gameNo)
    select = "SELECT id FROM app_page_sheet_game WHERE game_id = %s"
    cursor.execute(select, [gameId])
    id = cursor.fetchone()
    connection.commit()
    cursor.close()
    connection.close()
    if id is None:
        return None
    else :
        return id[0]


# Version optimisée (non testé)
# def getSheetGameIdByGameNo(gameNo):
#     with establish_db_connection() as connection:
#         with connection.cursor(buffered=True) as cursor:
#             gameId = findIdByGameNo(gameNo)
#             select_query = "SELECT id FROM app_page_sheet_game WHERE game_id = %s"
#             cursor.execute(select_query, [gameId])
#             result = cursor.fetchone()
#             return result[0] if result else None

def createSheetGamePage(gameNo):
    connection = establish_db_connection()
    cursor = connection.cursor()
    select_query = "SELECT schedule,sheetGame, matchNumber, clubNumberHome, clubNumberAway, scoreHome, scoreAway,competition,round FROM app_admin_game WHERE matchNumber = %s"
    cursor.execute(select_query, [gameNo])

    game = cursor.fetchone()

    select_query = "SELECT name FROM app_admin_team WHERE clubNumber = %s"
    cursor.execute(select_query, [game[3]])
    homeNameRaw = cursor.fetchone()[0]
    homeName = re.sub(r'\s', '-', re.sub(r'[^a-zA-Z0-9\s-]', '', homeNameRaw)).lower()

    select_query = "SELECT name FROM app_admin_team WHERE clubNumber = %s"
    cursor.execute(select_query, [game[4]])
    awayNameRaw = cursor.fetchone()[0]
    awayName = re.sub(r'\s', '-', re.sub(r'[^a-zA-Z0-9\s-]', '', awayNameRaw)).lower()
    slug = ""
    for truc in game[0].strftime("%Y-%m-%d %H:%M:%S").split(" ")[0].split("-"):
        slug += truc
    slug += "-" + re.sub(r'\s', '-',game[7].lower())

    slug += "-" + homeName
    slug += "-" + awayName
    # print(slug)
    # slug += "-"+("("+str(game[5]) + "-" + str(game[6])+")")
    insert_query = "INSERT INTO app_page_sheet_game (introduction, thanks, enabled, status, created_at, updated_at, shortDescription, bannerTitle, bannerAltDesktop, bannerAltMobile, bannerDesktop_id, bannerMobile_id, entityName, category,slug, game_id,name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

    # print(game[7] == "N3")
    if game[7] == "N3":
        category = "Championnat"
        name = " Journée du championat de National 3"
    elif game[7].lower() == "coupe de france":
        category = "Coup de France"
        name = " Tour de Coupe de France"
    else:
        category = "Coupe du Centre"
        name = " Tour de Coupe du Centre"

    if game[8] == 1:
        desc = str(game[8]) + "ère"
    else:
        desc = str(game[8]) + "ème"
    data_to_insert = (
        None,
        None,
        0,
        "Brouillon",
        game[0],
        game[0],
        desc + name,
        None,
        None,
        None,
        None,
        None,
        "is_sheet_game",
        category,
        slug,
        findIdByGameNo(gameNo),
        homeNameRaw + " - " + awayNameRaw + " (" + str(game[5]) + ":" + str(game[6]) + ")"
    )

    cursor.execute(insert_query, data_to_insert)

    connection.commit()
    cursor.close()
    connection.close()


# Version optimisée (non testé)
# import re
#
# def createSheetGamePage(gameNo):
#     with establish_db_connection() as connection:
#         with connection.cursor() as cursor:
#             select_query = "SELECT schedule, sheetGame, matchNumber, clubNumberHome, clubNumberAway, scoreHome, scoreAway, competition, round FROM app_admin_game WHERE matchNumber = %s"
#             cursor.execute(select_query, [gameNo])
#             game = cursor.fetchone()
#
#             select_query = "SELECT name FROM app_admin_team WHERE clubNumber = %s"
#             cursor.execute(select_query, [game[3]])
#             homeNameRaw = cursor.fetchone()[0]
#             homeName = re.sub(r'\s', '-', re.sub(r'[^a-zA-Z0-9\s-]', '', homeNameRaw)).lower()
#
#             cursor.execute(select_query, [game[4]])
#             awayNameRaw = cursor.fetchone()[0]
#             awayName = re.sub(r'\s', '-', re.sub(r'[^a-zA-Z0-9\s-]', '', awayNameRaw)).lower()
#
#             slug = game[0].strftime("%Y-%m-%d").replace("-", "") + "-" + re.sub(r'\s', '-', game[7].lower()) + "-" + homeName + "-" + awayName
#
#             if game[7] == "N3":
#                 category = "Championnat"
#                 name = " Journée du championnat de National 3"
#             elif game[7].lower() == "coupe de france":
#                 category = "Coup de France"
#                 name = " Tour de Coupe de France"
#             else:
#                 category = "Coupe du Centre"
#                 name = " Tour de Coupe du Centre"
#
#             desc = str(game[8]) + "ère" if game[8] == 1 else str(game[8]) + "ème"
#             description = desc + name
#
#             data_to_insert = (
#                 None,
#                 None,
#                 0,
#                 "Brouillon",
#                 game[0],
#                 game[0],
#                 description,
#                 None,
#                 None,
#                 None,
#                 None,
#                 None,
#                 "is_sheet_game",
#                 category,
#                 slug,
#                 findIdByGameNo(gameNo),
#                 f"{homeNameRaw} - {awayNameRaw} ({game[5]}:{game[6]})"
#             )
#
#             insert_query = "INSERT INTO app_page_sheet_game (introduction, thanks, enabled, status, created_at, updated_at, shortDescription, bannerTitle, bannerAltDesktop, bannerAltMobile, bannerDesktop_id, bannerMobile_id, entityName, category,slug, game_id,name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
#
#             cursor.execute(insert_query, data_to_insert)
#             connection.commit()
#



# createSheetGamePage(27227868)