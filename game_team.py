import requests
from connection import establish_db_connection


def insertGameTeam(ids):
    connection = establish_db_connection()
    cursor = connection.cursor()
    insert_query = "INSERT INTO game_team (game_id, team_id) VALUES (%s, %s)"
    cursor.execute(insert_query, [ids[0], ids[1]])
    connection.commit()
    cursor.close()
    connection.close()
